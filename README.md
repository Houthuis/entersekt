# Entersekt Technical Assessment by Henko Holtzhausen

The purpose of the assessment is to evaluate that the candidate has the
fundamental technical ability to be successful as a Systems Developer at Entersekt.

This application lists all of the files and folders of a given directory on the local filesystem.
It will give the full path, file size and whether it's a file or folder as JSON output.

## Building the project

Navigate to the project's root directory and run the following command with sudo powers:

    $ mvn clean package

## Dockerizing the application

Navigate to the project's root directory and run the following command with sudo powers:

    $ docker build --tag=entersekt:v1 .
    

## Running the application

    $ docker run -p 8080:8080 -t -i entersekt:v1


## Consuming the service

    $ curl http://localhost:8080/directory?path={directory}
    
Examples

    Linux

    $ curl http://localhost:8080/directory?path=/home/henko/dev/workspace
    
    Windows
    
    curl http://localhost:8080/directory?path=C%3A%5Cdev%5Cworkspace
    
Important! Windows paths must be URL encoded



package com.henko.entersekttechnicalassessment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EntersektTechnicalAssesmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(EntersektTechnicalAssesmentApplication.class, args);
	}
}

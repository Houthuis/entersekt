package com.henko.entersekttechnicalassessment.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.henko.entersekttechnicalassessment.data.Directory;
import com.henko.entersekttechnicalassessment.service.DirectoryLogic;

@RestController
public class DirectoryController {

    private DirectoryLogic directoryLogic = new DirectoryLogic();

    String ret = "Path is %s";

    @RequestMapping("directory")
    public Directory directory(@RequestParam(value = "path") String path) {

        return directoryLogic.getDirectoryContents(path);
    }
}

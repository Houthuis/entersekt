package com.henko.entersekttechnicalassessment.data;

import java.util.List;

public class Directory {

    private final String fullPath;
    private final long size;
    private final List<DirectoryFile> files;

    public Directory(String fullPath, long size, List<DirectoryFile> files) {
        this.fullPath = fullPath;
        this.size = size;
        this.files = files;
    }

    public String getFullPath() {
        return fullPath;
    }

    public long getSize() {
        return size;
    }

    public List<DirectoryFile> getFiles() {
        return files;
    }
}

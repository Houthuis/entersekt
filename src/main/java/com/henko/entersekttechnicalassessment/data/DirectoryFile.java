package com.henko.entersekttechnicalassessment.data;

public class DirectoryFile {

    private final String fullPath;
    private final long size;
    private final boolean isFile;

    public DirectoryFile(String fullPath, long size, boolean isFile) {
        this.fullPath = fullPath;
        this.size = size;
        this.isFile = isFile;
    }

    public String getFullPath() {
        return fullPath;
    }

    public long getSize() {
        return size;
    }

    public boolean isFile() {
        return isFile;
    }
}

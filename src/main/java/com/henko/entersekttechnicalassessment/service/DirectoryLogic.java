package com.henko.entersekttechnicalassessment.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;

import com.henko.entersekttechnicalassessment.data.Directory;
import com.henko.entersekttechnicalassessment.data.DirectoryFile;

public class DirectoryLogic {

    public Directory getDirectoryContents(String path) {

        long size = 0;
        List<DirectoryFile> fileList = new LinkedList<>();
        Path dir = getPath(path);

        try (DirectoryStream<Path> stream = Files.newDirectoryStream(dir)){
            for (Path sub : stream) {
                size += Files.size(sub);
                File subFile = sub.toFile();
                fileList.add(new DirectoryFile(subFile.getAbsolutePath(), Files.size(sub), subFile.isFile()));
            }
        } catch (IOException e) {
            throw new RuntimeException("Unable to find directory contents for path: " + path, e);
        }

        return new Directory(dir.toString(), size, fileList);

    }

    private Path getPath(String path) {
        Path baseDir = Paths.get(path);
        if (Files.notExists(baseDir)) {
            throw new RuntimeException("Unable to find a directory named " + path);
        }
        if (baseDir.toFile().isFile()) {
            throw new RuntimeException(path + " is a File, not a Directory");
        }
        return baseDir;
    }
}
